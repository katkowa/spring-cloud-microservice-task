package com.javagda19.microservicetask;

import com.javagda19.microservicetask.model.CreateTaskDto;
import com.javagda19.microservicetask.model.Task;
import com.javagda19.microservicetask.model.TaskDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.websocket.server.PathParam;
import java.util.List;

@RestController
@RequestMapping("/task")
public class TaskController {
    private final TaskService taskService;

    public TaskController(final TaskService taskService) {
        this.taskService = taskService;
    }

    @PostMapping("/create")
    @ResponseStatus(HttpStatus.CREATED)
    public Long createTask(@RequestBody CreateTaskDto dto) {
        return taskService.create(dto);
    }

    @GetMapping("/{id}")
    public TaskDto getTask(@PathVariable(name = "id") Long taskId) {
        return taskService.getById(taskId);
    }

    @GetMapping("/user/{id}")
    public List<Task> getTaskByUser(@PathVariable Long id) {
        return taskService.getByUser(id);
    }

    @DeleteMapping("/delete/user/{id}")
    public void deleteTaskByUserId(@PathVariable(name = "id") Long userId) {
        taskService.removeByUserId(userId);
    }
}
