package com.javagda19.microservicetask;

import com.javagda19.microservicetask.model.Task;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TaskRepository extends JpaRepository<Task, Long> {
    List<Task> findAllByOwnerId(Long userId);

    void removeByOwnerId(Long ownerId);
}
