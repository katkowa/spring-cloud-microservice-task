package com.javagda19.microservicetask.apiclient;

import com.javagda19.microservicetask.model.UserDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@Component
@FeignClient("microservice-user")
public interface UserApiClient {
    @GetMapping("/user/{id}")
    UserDto get(@PathVariable(name = "id") Long id);

}
