package com.javagda19.microservicetask;

import com.javagda19.microservicetask.apiclient.UserApiClient;
import com.javagda19.microservicetask.model.CreateTaskDto;
import com.javagda19.microservicetask.model.Task;
import com.javagda19.microservicetask.model.TaskDto;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Optional;

@Service
public class TaskService {
    private TaskRepository taskRepository;
    private UserApiClient userApiClient;

    public TaskService(final TaskRepository taskRepository, final UserApiClient userApiClient) {
        this.taskRepository = taskRepository;
        this.userApiClient = userApiClient;
    }

    public Long create(final CreateTaskDto dto) {
        try {
            userApiClient.get(dto.getOwnerId());
        } catch (Exception e) {
            throw new EntityNotFoundException("User owner can't be found");
        }
        Task task  = new Task();
        task.setDescription(dto.getDescription());
        task.setOwnerId(dto.getOwnerId());
        task = taskRepository.save(task);
        return task.getId();
    }

    public TaskDto getById(final Long taskId) {
        Optional<Task> taskOptional = taskRepository.findById(taskId);
        if (taskOptional.isPresent()) {
            Task task = taskOptional.get();
            TaskDto dto = new TaskDto();
            dto.setCreated(task.getCreated());
            dto.setDescription(task.getDescription());
            dto.setDone(task.isDone());
            dto.setOwnerId(task.getOwnerId());
            return dto;
        }
        throw new EntityNotFoundException();
    }

    public List<Task> getByUser(Long userId) {
        return taskRepository.findAllByOwnerId(userId);
    }

    @Transactional
    public void removeByUserId(Long userId) {
        taskRepository.removeByOwnerId(userId);
    }
}
